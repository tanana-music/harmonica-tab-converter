# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## v0.2.0

### Added
- Support for bend notes in both converters. Bend are notes ending with the `'` character.

### Changed
- Refactor in `converterNote` for diatonic to chromatic

## v0.1.1

### Added
- LICENSE
- This file
- `chromaticToDiatonic` implementation
- `diatonicToChromatic` implementation
- tests
