Harmonica tabs converter
========================
Converts a harmonica tab from chromatic to diatonic (given the diatonic scale) and
vice-versa.

## Usage

```javascript
import { chromaticToDiatonic } from 'harmonica-tab-converter'

chromaticToDiatonic({
    scale: 'C',
    tab: `-2 -2   -3  -4  <4  -5    6    -6  6  -5 -5
       Yesterday, all my troubles seemed so far away`
})
```
Yes, it does not care if you have lyrics with your tabs.

### Notation
It is using `-1` for a draw in the hole 1. And the symbol `<`
to represent when you push the slider in the chromatic harmonica.
Also, the `'` character in the end of a note (as in `-3'`>) represents
a bend and it's ignored and preserved in the converted tab.

### API

The lib exports 2 functions: `chromaticToDiatonic({ scale, tab })` and 
`diatonicToChromatic({ scale, tab })` with the same options.

### Installation
Just run `npm install harmonica-tab-converter` and use it.

## License
MIT License