export const REGEXP_TABS = {
  chromatic: "(-|<)*([0-9]+)'*",
  diatonic: "-*([0-9]+)'*"
}

export const CHROMATIC_NOTES = {
  blow: ['C', 'E', 'G', 'C'],
  draw: ['D', 'F', 'A', 'B']
}

export const DIATONIC_HARMONICA_MAP = {
  blow: [1, 3, 5, 1, 3, 5, 1, 3, 5, 1],
  draw: [2, 5, 7, 2, 4, 6, 7, 2, 4, 6]
}

export const DIATONIC_HARMONICA_OCTAVES = [0, 3, 6]
