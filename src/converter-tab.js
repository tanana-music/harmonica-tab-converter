import { REGEXP_TABS } from './constants'
import { convertNote as convertChromatic } from './note-converters/chromatic-to-diatonic'
import { convertNote as convertDiatonic } from './note-converters/diatonic-to-chromatic'

const convertNote = {
  chromatic: convertChromatic,
  diatonic: convertDiatonic
}

export const convertIfNote = ({ type, scale, str }) => {
  if (!str.match(REGEXP_TABS[type])) {
    return str
  }
  return convertNote[type]({ scale, tabNote: str })
}

export const convertLine = ({ type, line, scale }) => {
  return line.split(' ').map(str => convertIfNote({ type, scale, str })).join(' ')
}

export const converterTabCreator = type => ({ tab, scale }) => {
  return tab.split('\n').map((line) => {
    const noteOrWhiteSpaceWholeLine = `^(${REGEXP_TABS[type]}|\\s)+$`
    if (line.match(noteOrWhiteSpaceWholeLine)) {
      return convertLine({ type, line, scale })
    }
    return line
  }).join('\n')
}
