import { converterTabCreator } from './converter-tab'

export const chromaticToDiatonic = converterTabCreator('chromatic')
export const diatonicToChromatic = converterTabCreator('diatonic')
