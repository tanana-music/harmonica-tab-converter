import { Scale } from '@tonaljs/tonal'

import {
  DIATONIC_HARMONICA_OCTAVES,
  CHROMATIC_NOTES,
  DIATONIC_HARMONICA_MAP
} from '../constants'
import { breath, tabHole, bendSign } from '../tab-read'

export const accidentChromaticTab = note => note.match(/</) ? '#' : ''
export const harmonicaOctave = (note) => {
  const holesToNextOctave = CHROMATIC_NOTES.blow.length
  return parseInt((tabHole(note) - 1) / holesToNextOctave)
}
export const noteABC = (tabNote) => {
  const chromaticBreath = breath(tabNote)
  const notesMap = CHROMATIC_NOTES[chromaticBreath]
  const note = notesMap[(tabHole(tabNote) - 1) % notesMap.length]
  const accident = accidentChromaticTab(tabNote)
  return `${note}${accident}`
}
export const gradeFromScale = ({ scale, tabNote }) => {
  const note = noteABC(tabNote)
  const { notes } = Scale.get(`${scale} major`)
  return notes.indexOf(note) + 1
}

export const diatonicHoleFromBreath = ({ grade, tabNote, breath }) => {
  const searchStart = DIATONIC_HARMONICA_OCTAVES[harmonicaOctave(tabNote)]
  return DIATONIC_HARMONICA_MAP[breath].indexOf(grade, searchStart) + 1
}

export const convertNote = ({ scale, tabNote }) => {
  const grade = gradeFromScale({ scale, tabNote })
  const bend = bendSign(tabNote)
  let diatonicHole = diatonicHoleFromBreath({ grade, tabNote, breath: 'blow' })
  let diatonicBreath = ''
  if (!diatonicHole) {
    diatonicBreath = '-'
    diatonicHole = diatonicHoleFromBreath({ grade, tabNote, breath: 'draw' })
  }
  return `${diatonicBreath}${diatonicHole}${bend}`
}
