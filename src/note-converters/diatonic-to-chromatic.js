import { Scale } from '@tonaljs/tonal'

import {
  DIATONIC_HARMONICA_OCTAVES,
  CHROMATIC_NOTES,
  DIATONIC_HARMONICA_MAP
} from '../constants'
import { breath, tabHole, bendSign } from '../tab-read'

const removeAccidentSign = (note) => note.split(/#|b/).join('')
const accidentSign = (note) => note.match(/#|b/) ? '<' : ''
const flatAccident = (note) => note.match(/b/)
const breathSymbol = (breath) => breath === 'draw' ? '-' : ''
const octave = (diatonicHole) => DIATONIC_HARMONICA_OCTAVES
  .findIndex(hole => hole >= diatonicHole) - 1
const firstOctaveChromaticHole = (breath, note) => {
  const noteWithoutAccident = removeAccidentSign(note)
  let hole = CHROMATIC_NOTES[breath].indexOf(noteWithoutAccident) + 1
  if (!hole) return 0
  if (flatAccident(note)) {
    hole -= 1
  }
  return hole
}
const chromaticNote = ({ scale, tabNote }) => {
  const breathDiatonic = breath(tabNote)
  const diatonicHole = tabHole(tabNote)
  const grade = DIATONIC_HARMONICA_MAP[breathDiatonic][diatonicHole - 1]
  const { notes } = Scale.get(`${scale} major`)
  return notes[grade - 1]
}

const breathAndHole = ({ scale, tabNote }) => {
  const note = chromaticNote({ scale, tabNote })
  const diatonicHole = tabHole(tabNote)
  const octaveIndex = octave(diatonicHole)
  let chromaticBreath = 'blow'
  let chromaticHole = firstOctaveChromaticHole(chromaticBreath, note)
  if (!chromaticHole) {
    chromaticBreath = 'draw'
    chromaticHole = firstOctaveChromaticHole(chromaticBreath, note)
  }
  chromaticHole += CHROMATIC_NOTES.blow.length * octaveIndex
  return {
    chromaticBreath: breathSymbol(chromaticBreath),
    chromaticHole
  }
}

export const convertNote = ({ scale, tabNote }) => {
  const bend = bendSign(tabNote)
  const note = chromaticNote({ scale, tabNote })
  const accident = accidentSign(note)
  const { chromaticBreath, chromaticHole } = breathAndHole({ scale, tabNote })
  return `${accident}${chromaticBreath}${chromaticHole}${bend}`
}
