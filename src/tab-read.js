export const breath = note => (note.match(/-/) ? 'draw' : 'blow')
export const tabHole = note => parseInt(note.match(/\d+/)[0])
export const bendSign = (note) => note.match(/'$/) ? "'" : ''
