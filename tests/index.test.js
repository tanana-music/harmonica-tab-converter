import {
  diatonicToChromatic,
  chromaticToDiatonic
} from '../index.js'

describe('the lib', () => {
  describe('diatonicToChromatic', () => {
    it('is a function', () => {
      expect(typeof diatonicToChromatic).toEqual('function')
    })
  })
  describe('chromaticToDiatonic', () => {
    it('is a function', () => {
      expect(typeof chromaticToDiatonic).toEqual('function')
    })
  })
})
