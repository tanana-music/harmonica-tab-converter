import { converterTabCreator } from '../../src/converter-tab'
import * as chromaticToDiatonic from '../../src/note-converters/chromatic-to-diatonic'
import * as diatonicToChromatic from '../../src/note-converters/diatonic-to-chromatic'

jest.mock('../../src/note-converters/chromatic-to-diatonic', () => ({
  convertNote: jest.fn().mockReturnValue('1')
}))
jest.mock('../../src/note-converters/diatonic-to-chromatic', () => ({
  convertNote: jest.fn().mockReturnValue('1')
}))

let converter, convertedTab
const tab = '2 2   2  2'
const scale = 'C'

describe('converterTabCreator', () => {
  describe('when the type does not matter', () => {
    beforeAll(() => {
      converter = converterTabCreator('diatonic')
    })
    it('keeps original white space', () => {
      convertedTab = converter({ scale, tab })
      expect(convertedTab).toEqual(tab.split('2').join('1'))
    })
    describe('when lyrics are present', () => {
      it('only converts the tabs', () => {
        expect(converter({
          scale,
          tab: `-2 -2   -3  -4  <4  -5    6    -6  6  -5 -5
                 Yesterday, all my troubles seemed so far away`
        })).toMatch('Yesterday, all my troubles seemed so far away')
      })
    })
  })
  describe('when the type is chromatic', () => {
    beforeAll(() => {
      converter = converterTabCreator('chromatic')
      convertedTab = converter({ scale, tab })
    })
    it('calls the correct note converter', () => {
      expect(chromaticToDiatonic.convertNote).toHaveBeenCalled()
      expect(chromaticToDiatonic.convertNote)
        .toHaveBeenCalledWith({ scale, tabNote: '2' })
    })
  })
  describe('when the type is diatonic', () => {
    beforeAll(() => {
      converter = converterTabCreator('diatonic')
      convertedTab = converter({ scale, tab })
    })
    it('calls the correct note converter', () => {
      expect(diatonicToChromatic.convertNote).toHaveBeenCalled()
      expect(diatonicToChromatic.convertNote)
        .toHaveBeenCalledWith({ scale, tabNote: '2' })
    })
  })
})
