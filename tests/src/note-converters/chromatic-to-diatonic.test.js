import {
  accidentChromaticTab,
  harmonicaOctave,
  noteABC,
  gradeFromScale,
  diatonicHoleFromBreath,
  convertNote
} from '../../../src/note-converters/chromatic-to-diatonic'

describe('accidentChromaticTab', () => {
  describe('when the note has a < sign', () => {
    it('returns a #', () => {
      expect(accidentChromaticTab('<3')).toEqual('#')
      expect(accidentChromaticTab('<5')).toEqual('#')
    })
  })
  describe('when the note does not have a < sign', () => {
    it('returns empty string', () => {
      expect(accidentChromaticTab('2')).toEqual('')
      expect(accidentChromaticTab('6')).toEqual('')
    })
  })
})

describe('harmonicaOctave', () => {
  it('returns 0 for the first notes', () => {
    expect(harmonicaOctave('1')).toEqual(0)
    expect(harmonicaOctave('-3')).toEqual(0)
    expect(harmonicaOctave('-4')).toEqual(0)
  })
  it('returns 1 for the notes in the second octave', () => {
    expect(harmonicaOctave('-5')).toEqual(1)
    expect(harmonicaOctave('6')).toEqual(1)
  })
  it('returns 2 for the notes in the third octave', () => {
    expect(harmonicaOctave('-9')).toEqual(2)
    expect(harmonicaOctave('10')).toEqual(2)
  })
})

describe('noteABC', () => {
  describe('when given the blow hole 1', () => {
    it('returns C', () => {
      expect(noteABC('1')).toEqual('C')
    })
  })
  describe('when given the draw hole 3', () => {
    it('returns A', () => {
      expect(noteABC('-3')).toEqual('A')
    })
  })
  describe('when given the draw hole 3 with the slider', () => {
    it('returns F#', () => {
      expect(noteABC('<-6')).toEqual('F#')
    })
  })
})

describe('gradeFromScale', () => {
  it('returns the position of the note in the scale', () => {
    expect(gradeFromScale({ scale: 'C', tabNote: '1' })).toEqual(1)
    expect(gradeFromScale({ scale: 'C', tabNote: '-1' })).toEqual(2)
  })
})

describe('diatonicHoleFromBreath', () => {
  describe('when the breath is wrong', () => {
    it('returns 0', () => {
      expect(diatonicHoleFromBreath({
        grade: 1, tabNote: '1', breath: 'draw'
      })).toEqual(0)
    })
  })
  describe('when the breath is right', () => {
    it('returns the correspondent hole', () => {
      expect(diatonicHoleFromBreath({
        grade: 1, tabNote: '1', breath: 'blow'
      })).toEqual(1)
    })
  })
})

describe('convertNote', () => {
  const scale = 'G'
  beforeAll(() => {
    jest.mock('@tonaljs/tonal', () => ({
      Scale: {
        get: jest.fn().mockReturnValue({
          // G scale
          notes: ['G', 'A', 'B', 'C', 'D', 'E', 'F#']
        })
      }
    }))
  })
  describe('when passing a diatonic scale that has the given note', () => {
    describe('when the note has a bend', () => {
      it('returns the correspondent note ignoring the bend', () => {
        // this is a C
        expect(convertNote({ scale, tabNote: "5'" })).toEqual("-5'")
      })
    })
    describe('when the note is in the first octave', () => {
      it('returns the correspondent note in a given diatonic scale', () => {
        // this is a C
        expect(convertNote({ scale, tabNote: '4' })).toEqual('-5')
        // this is a D
        expect(convertNote({ scale, tabNote: '-1' })).toEqual('3')
        // this is a F#
        expect(convertNote({ scale, tabNote: '<-2' })).toEqual('-3')
      })
    })
    describe('when the note is NOT in the first octave', () => {
      it('returns the correspondent note in a given diatonic scale', () => {
        // this is a F#
        expect(convertNote({ scale, tabNote: '<-6' })).toEqual('-7')
      })
    })
  })
})
