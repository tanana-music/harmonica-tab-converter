import {
  convertNote
} from '../../../src/note-converters/diatonic-to-chromatic'

describe('convertNote', () => {
  describe('when the note does not have an accident', () => {
    describe('when the note is in the first octave', () => {
      it('returns the correspondent note in a given diatonic scale', () => {
        // this is an A
        expect(convertNote({ scale: 'G', tabNote: '-1' })).toEqual('-3')
        // this is a G
        expect(convertNote({ scale: 'G', tabNote: '1' })).toEqual('3')
        // this is a D
        expect(convertNote({ scale: 'C', tabNote: '-1' })).toEqual('-1')
      })
    })
    describe('when the note is NOT in the first octave', () => {
      it('returns the correspondent note in a given diatonic scale', () => {
        // this is a E
        expect(convertNote({ scale: 'C', tabNote: '5' })).toEqual('6')
      })
    })
    describe('when the note has a bend', () => {
      it('returns the correspondent note ignoring the bend', () => {
        // this is an A
        expect(convertNote({ scale: 'G', tabNote: "1'" })).toEqual("3'")
      })
    })
  })
  describe('when the note does have a # accident', () => {
    it('returns the correspondent note in a given diatonic scale', () => {
      // this is a F#
      expect(convertNote({ scale: 'G', tabNote: '-3' })).toEqual('<-2')
    })
  })
  describe('when the note does have a b accident', () => {
    it('returns the correspondent note in a given diatonic scale', () => {
      // this is a Bb and should be read as A#
      expect(convertNote({ scale: 'Bb', tabNote: '1' })).toEqual('<-3')
    })
  })
})
