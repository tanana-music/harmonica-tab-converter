import { breath, tabHole, bendSign } from '../../src/tab-read'

describe('breath', () => {
  describe('when the note have a - sign', () => {
    it('is a draw breath', () => {
      expect(breath('-1')).toEqual('draw')
      expect(breath('-8')).toEqual('draw')
    })
    it('ignores bend', () => {
      expect(breath("-8'")).toEqual('draw')
    })
  })
  describe('when the note does not have a - sign', () => {
    it('is a blow breath', () => {
      expect(breath('2')).toEqual('blow')
      expect(breath('5')).toEqual('blow')
    })
    it('ignores bend', () => {
      expect(breath("5'")).toEqual('blow')
    })
  })
})

describe('tabHole', () => {
  it('returns the number from the note', () => {
    expect(tabHole('1')).toEqual(1)
    expect(tabHole('-2')).toEqual(2)
    expect(tabHole("-2'")).toEqual(2)
    expect(tabHole('>4')).toEqual(4)
    expect(tabHole('<-10')).toEqual(10)
  })
})
describe('bendSign', () => {
  it('returns the sign when there is a bend', () => {
    expect(bendSign("1'")).toEqual("'")
  })
  it('returns emtpy when there is not a bend', () => {
    expect(bendSign('2')).toEqual('')
  })
})
